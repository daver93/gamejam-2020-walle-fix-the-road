﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Build : MonoBehaviour
{
    public GameObject[] parts;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void BuildPiece(string numberOfPartObject)
    {
        

        int partNumber = int.Parse(numberOfPartObject) -1;
        Debug.Log("Part Number: " + partNumber);

        GameObject part = parts[partNumber].gameObject;

        Debug.Log("Part Name: " + part.gameObject.name);


        part.gameObject.SetActive(true);
    }
}
