﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Here");

        if (other.name.Equals("Part1"))
        {
            GetComponent<Build>().BuildPiece("1");
        }
        if (other.name.Equals("Part2"))
        {
            GetComponent<Build>().BuildPiece("2");
        }
        if (other.name.Equals("Part3"))
        {
            GetComponent<Build>().BuildPiece("3");
        }
        if (other.name.Equals("Part4"))
        {
            GetComponent<Build>().BuildPiece("4");
        }
        if (other.name.Equals("Part5"))
        {
            GetComponent<Build>().BuildPiece("5");
        }
        if (other.name.Equals("Part6"))
        {
            GetComponent<Build>().BuildPiece("6");
        }
        if (other.name.Equals("Part7"))
        {
            GetComponent<Build>().BuildPiece("7");
        }
        if (other.name.Equals("Part8"))
        {
            GetComponent<Build>().BuildPiece("8");
        }
        if (other.name.Equals("Part9"))
        {
            GetComponent<Build>().BuildPiece("9");
        }
        if (other.name.Equals("Part10"))
        {
            GetComponent<Build>().BuildPiece("10");
        }

        Destroy(other.gameObject);
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKey("up"))
        {
            // Move the object forward along its z axis 1 unit/second.
            transform.Translate(Vector3.forward * Time.deltaTime * 2);
        }
        if (Input.GetKey("down"))
        {
            // Move the object backward along its z axis 1 unit/second.
            transform.Translate(Vector3.back * Time.deltaTime * 2);
        }
        if (Input.GetKey("left"))
        {
            // Move the object left along its z axis 1 unit/second.
            transform.Translate(Vector3.left * Time.deltaTime * 2);
        }
        if (Input.GetKey("right"))
        {
            // Move the object right along its z axis 1 unit/second.
            transform.Translate(Vector3.right * Time.deltaTime * 2);
        }


        // Move the object upward in world space 1 unit/second.
        //transform.Translate(Vector3.up * Time.deltaTime, Space.World);
    }

    

}
