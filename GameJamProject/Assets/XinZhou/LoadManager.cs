﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadManager : MonoBehaviour
{
    public GameObject loadScrene;
    public Slider slider;
    public Text text;

    public void LoadNextScene() {
        StartCoroutine(LoadMainGame());
    }
    IEnumerator LoadMainGame()
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + 1);

        operation.allowSceneActivation = false;
        while (!operation.isDone) {
            slider.value = operation.progress;
            text.text = operation.progress * 100 + "%";
            if (operation.progress >= 0.9f) {
                slider.value = 1;
                text.text = "Press AnyKey To continue";
                if (Input.anyKeyDown) {
                    operation.allowSceneActivation = true;
                }

            }
            yield return null;
        }

    } 
        
    
}
