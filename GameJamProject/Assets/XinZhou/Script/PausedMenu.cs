﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PausedMenu : MonoBehaviour
{
    public GameObject ingameMenu;
    // Start is called before the first frame update
    public void OnPause()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Time.timeScale = 0;
            ingameMenu.SetActive(true);
        }
    }
    public void OnResume()
    {
        Time.timeScale = 1f;
        ingameMenu.SetActive(false);

    }
    public void OnRestart()
    {
        SceneManager.LoadScene(1);
        Time.timeScale = 1f;

    }
    public void BackToMainMenu() {
        SceneManager.LoadScene(0);
    }
}
