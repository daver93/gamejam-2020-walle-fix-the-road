﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    private AudioSource audioSource;
    private float preVolume;
    private bool muteState;
    

    public GameObject bar;
    // Start is called before the first frame update
    public void PlayGame() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    public void QuitGame() {
        Application.Quit();
        
    }
    public void Volume(float newVolume) {
        audioSource.volume = newVolume;
        muteState = false;
    }
    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.volume = 0;
        muteState = false;
        preVolume = audioSource.volume;
    }

  
}
